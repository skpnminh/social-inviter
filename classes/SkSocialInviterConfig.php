<?php
bx_import('BxDolConfig');

class SkSocialInviterConfig extends BxDolConfig {

	private $_iApiAppId;
	private	$_sApiServiceUrl;
	private $_sApiLoginUrl;

	function SkSocialInviterConfig($aModule) {
	    parent::BxDolConfig($aModule);
		$this->_sApiLoginUrl = getParam('sk_social_inviter_api_login_url');
		$this->_sApiServiceUrl = getParam('sk_social_inviter_api_service_url');
		$this->_iApiAppId = getParam('sk_social_inviter_app_id');
	}
	
	function getAppId(){
		return $this->_iApiAppId;
	}
	function getApiLoginUrl(){
		return $this->_sApiLoginUrl;
	}
	function getApiServiceUrl(){
		return $this->_sApiServiceUrl;
	}
}

?>
