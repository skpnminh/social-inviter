<?php
bx_import('BxDolModuleDb');

class SkSocialInviterDb extends BxDolModuleDb {

	var $_sTablePrefix;
	var $_sTableNetworks;
	var $_sTableUsers;

	function SkSocialInviterDb(&$oConfig) {
		parent::BxDolModuleDb();
        $this->_sTablePrefix = $oConfig->getDbPrefix();
		$this->_sTableNetworks = $this->_sTablePrefix.'networks';
		$this->_sTableUsers = $this->_sTablePrefix.'users';
    }
	
	
	function getSettings(){
		return (int) $this->getOne("SELECT `ID` FROM `sys_options_cats` WHERE `name` = 'Sandklock Social Inviter Setting' LIMIT 1");
	}
	
	function getApiSettings()
    {
        return (int) $this->getOne("SELECT `ID` FROM `sys_options_cats` WHERE `name` = 'Sandklock Social Inviter Api Setting' LIMIT 1");
    }
	
	function getAllNetworks(){
		$sql = " SELECT * FROM `{$this->_sTableNetworks}` ORDER BY `order` ASC";
		return $this->getAll($sql);
	}
	
	function getEnabledNetworks(){
		$sql = " SELECT * FROM `{$this->_sTableNetworks}` WHERE `status` = 'enabled' ORDER BY `order` ASC";
		return $this->getAll($sql);
	}
	
	function getProfileByEmail($sEmail){
		$sEmail = process_db_input($sEmail);
		$sql = " SELECT * FROM `profiles` where `Email` = '{$sEmail}' ";
		$result = $this->getRow($sql);
		
		return !empty($result) ? $result : false;
	}
	
	function createNetworkFriend($jFriendList,$sNetwork,$iId){
		$aFriendList = json_decode($jFriendList,true);
		$iLength = count($aFriendList);
		$iCount = 0;
		$sValue = '';
		foreach($aFriendList as $friend_id){
			$iCount++;
			if($iCount < $iLength)
				$sValue .= "('{$sNetwork}','{$friend_id}','{$iId}'),";
			else
				$sValue .= "('{$sNetwork}','{$friend_id}','{$iId}')";
		}
		$sql = " INSERT INTO `{$this->_sTableUsers}`(`network`,`friend_identity`,`profile_id`) VALUES{$sValue}"; 
		
		return $this->query($sql);
	}
	
	function getTotalFriends($sNetwork,$iId){
		$sql = " SELECT COUNT(DISTINCT `friend_identity`) FROM `{$this->_sTableUsers}` WHERE `profile_id` = '{$sId}' and `network` = '{$sNetwork}' ";
		return (int) $this->getOne($sql);
	}
	
	function getTotalInvitations($iId){
		$sql = " SELECT COUNT(*) FROM `{$this->_sTableUsers}` WHERE `profile_id` = '{$sId}' ";
		return (int) $this->getOne($sql);
		
	}
	
	function updateNetworkOrder($aNetwork){
		$order = 1;
		$sWhenSql = '';
		foreach($aNetwork as $network){		
			$network = process_db_input($network);
			$sWhenSql .= " WHEN '{$network}' THEN {$order} ";
			$order++;
		}
		$sql = "UPDATE `{$this->_sTableNetworks}` SET `order` = CASE `name` {$sWhenSql} END";
				
		return $this->query($sql);
	}
	function updateNetworkStatus($sNetwork,$sStatus){
		$sNetwork = process_db_input($sNetwork);
		$sStatus = process_db_input($sStatus);
		$sql = " UPDATE `{$this->_sTableNetworks}` SET `status` = '{$sStatus}' WHERE `name` = '{$sNetwork}' ";
		return $this->query($sql);
	}
}

?>
