<?php

bx_import('BxDolModule');

class SkSocialInviterModule extends BxDolModule{

	var $_iVisitorID;
	var $_sModuleUri;

    function SkSocialInviterModule(&$aModule){        
        parent::BxDolModule($aModule); 
		$this->_iVisitorID = (isMember()) ? (int) $_COOKIE['memberID'] : 0;
		$this->_sModuleUri = BX_DOL_URL_ROOT . $this->_oConfig->getBaseUri();
    }
	
	function actionTest(){
		echoDbg(BxDolSession::getInstance()->getValue('si_data'));
	}
	
	function actionAdministration($sUrl = ''){
		if(!$this->isAdmin())
		{
			$this->_oTemplate->displayAccessDenied();
			return;
		}
		$this->_oTemplate->pageStart();
		$this->_oTemplate->addAdminCss(array('manage_network.css'));
		$this->_oTemplate->addAdminJs(array('jquery-ui.js','manage_network.js'));
		$aMenu = array(
			'settings' => array(
				'title' => _t('_sk_social_inviter_caption_settings'),
				'href' => $this->_sModuleUri . 'administration/settings',
				'_func' => array(
					'name' => 'getAdministrationSettings', 
					'params' => array()
				),
			),
			'api_settings' => array(
				'title' => _t('_sk_social_inviter_caption_api_settings'),
				'href' => $this->_sModuleUri . 'administration/api_settings',
				'_func' => array(
					'name' => 'getAdministrationApiSettings',
					'params' => array()
				),
			),
			'manage_network' => array(
				'title' => _t('_sk_social_inviter_caption_manage_network'),
				'href' => $this->_sModuleUri . 'administration/manage_network',
				'_func' => array(
					'name' => 'actionAdministrationManageNetwork', 
					'params' => array()
				),
			),
		);
		if(empty($aMenu[$sUrl]))
			$sUrl = 'settings';
		$aMenu[$sUrl]['active'] = 1;
		$sContent = call_user_func_array(array($this, $aMenu[$sUrl]['_func']['name']), $aMenu[$sUrl]['_func']['params']);
		echo $this->_oTemplate->adminBlock($sContent, _t('_sk_social_inviter_title_admin'), $aMenu);
		$this->_oTemplate->pageCodeAdmin(_t('_sk_social_inviter_page_admin'));
	}
	
	function actionAdministrationManageNetwork(){
		
		$aNetwork = $this->_oDb->getAllNetworks();
		
		if(!$aNetwork){
			$aVar['content'] = MsgBox(_t('_sk_social_inviter_db_error'));
			return $this->_oTemplate->parseHtmlByName('default_padding',$aVar);
		}
		
		foreach($aNetwork as $key=>$network){
			$aNetwork[$key]['image_url'] = $this->_oTemplate->getImageUrl($network['logo']);
			$aNetwork[$key]['loading_image'] = $this->_oTemplate->getImageUrl('updating.gif');
		}
		$aVar = array(
			'bx_repeat:networks' => $aNetwork,
		);
		
		$sContent = $this->_oTemplate->parseHtmlByName('block_network_admin',$aVar);
		
		$aContent = array(
			'content' => $sContent
		);
		
		return $this->_oTemplate->parseHtmlByName('default_padding',$aContent);
	}
	
	function actionInvite($sExpired = ''){
	
		$this->checkLogged();
		//$aFriendList = array();
		/*
		if($sExpired == 'not_expired'){
			$aNetworkUser = $this->_oDb->getInviterUser($this->_iVisitorID,$sNetwork);
			
			if($aNetworkUser && ($aNetworkUser['token_expired_time'] - time() > 0)){
				$aRequestHeader = array(
					'http' => array(
						'method' => 'GET'
					)
				);
				$sUrl = $this->_oConfig->getApiServiceUrl().$aNetworkUser['network'].'/getfriend?sk_token='.$aNetworkUser['sk_token'];
			
				$oContext = stream_context_create($aRequestHeader);
			
				$oResult = file_get_contents($sUrl,false,$oContext);
				$aFriendList = json_decode($oResult,true);
			}else
				header('location:'.$this->_oConfig->getApiLoginUrl().$sNetwork.'?app_id='.$this->_oConfig->getAppId(););
		}else{
		*/
		
		$sToken = bx_get('sk_token');
		$sNetwork = bx_get('network');
		
		$aRequestHeader = array(
			'http' => array(
				'method' => 'GET'
			)
		);
		
		$sUrl = $this->_oConfig->getApiServiceUrl().$sNetwork.'/getfriend?sk_token='.$sToken;
		
		$oContext = stream_context_create($aRequestHeader);
		
		$oResult = file_get_contents($sUrl,false,$oContext);
		$aFriendList = json_decode($oResult,true);		
		//}
		
		if(count($aFriendList) > 0){
		
			$session = BxDolSession::getInstance();
			$session->setValue('si_data',array('network' => $sNetwork,'sk_token' => $sToken));
		
			foreach($aFriendList as $key=>$value){
				if($sNetwork == 'google' || $sNetwork == 'live'){
					if(empty($value['email'])){
						unset($aFriendList[$key]);
						continue;
					}
				}
				$aFriendList[$key]['identity'] = $sNetwork == 'tumblr' ? $value['url'] : $value['id'];
				$aFriendList[$key]['receiver'] = $this->getReceiver($sNetwork,$value);
			}
			
			$aVar = array(
				'js_content' => "
								window.opener.showSocialInviterBlock('".addslashes(json_encode($aFriendList))."');
								window.close();
								"
			);
			
			echo $this->_oTemplate->parseHtmlByName('invite.html',$aVar);
			
		}else{
		
			echo $this->_oTemplate->parseHtmlByName('invite.html',array(
				'js_content' => "
								window.opener.showSocialInviterError('"._t('_sk_social_inviter_error')."');
								window.close();
								"
				)
			);
			
		}
	}
	
	function actionAjaxMode($sAction){
	
		$sRetHtml = '';
		if(!bx_get('ajaxmode'))
			$sRetHtml = 'Access denied';
		else
		{
			switch($sAction)
			{
				case 'order_network':
					$sRetHtml = $this->updateNetworkOrder();
					break;
				case 'status_network':
					$sRetHtml = $this->updateNetworkStatus();
					break;
				case 'friend_list_block':
					$sRetHtml = $this->genFriendListBlock(bx_get('list'));
					break;
				case 'message_form':
					$sRetHtml = $this->genMessageForm(bx_get('list_id'),bx_get('username'));
					break;
				case 'send_invitation':
					$sRetHtml = $this->sendInvitation(bx_get('list'),bx_get('message'),bx_get('subject'));
					break;
				case 'popup_message':
					$sRetHtml = $this->genPopupMessage(bx_get('message'));
				default:
					break;
			}
		}
		echo $sRetHtml;
		
	}
		
	function actionHome(){
		$this->_oTemplate->pageStart();
		echo "hello";
		$this->_oTemplate->pageCode(_t('_sk_social_inviter_home_page'),true);
	}
	
	function getAdministrationSettings()
	{
		$iId = $this->_oDb->getSettings();
		if(empty($iId))
			return MsgBox(_t('_sys_request_page_not_found_cpt'));
		bx_import('BxDolAdminSettings');
		$mixedResult = '';
		if(isset($_POST['save']) && isset($_POST['cat']))
		{
			$oSettings = new BxDolAdminSettings($iId);
			$mixedResult = $oSettings->saveChanges($_POST);
		}
		$oSettings = new BxDolAdminSettings($iId);
		$sResult = $oSettings->getForm();
		if($mixedResult !== true && !empty($mixedResult))
			$sResult = $mixedResult . $sResult;
		return $sResult;
	}
	
	function getAdministrationApiSettings()
	{
		$iId = $this->_oDb->getApiSettings();
		if(empty($iId))
			return MsgBox(_t('_sys_request_page_not_found_cpt'));
		bx_import('BxDolAdminSettings');
		$mixedResult = '';
		if(isset($_POST['save']) && isset($_POST['cat']))
		{
			$oSettings = new BxDolAdminSettings($iId);
			$mixedResult = $oSettings->saveChanges($_POST);
		}
		$oSettings = new BxDolAdminSettings($iId);
		$sResult = $oSettings->getForm();
		if($mixedResult !== true && !empty($mixedResult))
			$sResult = $mixedResult . $sResult;
		return $sResult;
	}
	
	function serviceGenInviterBlock($iId = 0){
		$aNetwork = $this->_oDb->getEnabledNetworks();
		
		foreach($aNetwork as $key=>$value){
			$aNetwork[$key]['img_src'] = $this->_oTemplate->getImageUrl($value['logo']);
			$aNetwork[$key]['login_url'] = $this->_oConfig->getApiLoginUrl().$value['name'].'?app_id='.$this->_oConfig->getAppId();
		}
		
		$aVar = array(
			'bx_repeat:networks' => $aNetwork,
		);
		
		echo $this->_oTemplate->parseHtmlByName('block_social_inviter.html',$aVar);
	}
	

	function sendInvitation($jReceiverList,$sMessage,$sTitle){
		$session = BxDolSession::getInstance();
		$aData = $session->getValue('si_data');
		
		switch($aData['network']){
			case 'google':
			case 'live':
				$aEmail = json_decode($jReceiverList);
				$aRes = array();
				foreach($aEmail as $sEmail){
					$aRes[] = $this->sendMail($sEmail);
				}
				return 'You have sent invitation mail to '.count($aRes).' friend(s)';
			break;
			case 'tumblr':
			case 'lastfm':
			case 'plurk':
			case 'twitter':
			case 'linkedin':
			case 'mailru':
				$aPostData = array(
					'message' => $sMessage,
					'friend_id' => implode(',',json_decode($jReceiverList,true)),
				);
				if(bx_get('subject'))
					$aPostData['title'] = bx_get('subject');
				$aResult = $this->makeSendMessageRequest($aData['network'],$aData['sk_token'],$aPostData);
				if((isset($aResult[0]) && $aResult[0]['success'] == 1) ||(isset($aResult) && $aResult['success'] == 1)){
					$this->_oDb->createNetworkFriend($jReceiverList,$aData['network'],$this->_iVisitorID);
					return "You have sent message to friends successfully";
				}
				else
					return "Failed to send message";
			break;
		}
	}
	
	function updateNetworkOrder(){
		$aNetwork = bx_get('network');
		return $this->_oDb->updateNetworkOrder($aNetwork);
	}
	
	function updateNetworkStatus(){
		$sNetwork = bx_get('network');
		$sStatus = bx_get('status');
		return $this->_oDb->updateNetworkStatus($sNetwork,$sStatus);
	}
	
	function genPopupMessage($sMessage){
		$sContent = MsgBox($sMessage);
		
		$sCaptionItem = <<<BLAH
		<div class="dbTopMenu">
			<i class="login_ajx_close sys-icon remove"></i>
		</div>
BLAH;
		return $GLOBALS['oFunctions']->transBox(DesignBoxContent('Message', $sContent, 11,$sCaptionItem),true);
	}
	
	function genMessageForm($jFriendId,$jFriendList){
	
		$aFriendList = json_decode($jFriendList,true);
		$aParsedList = array();
		
		foreach($aFriendList as $username){
			$aParsedList[]['username'] = $username;
		}
		
		$session = BxDolSession::getInstance();
		$aData = $session->getValue('si_data');
		
		$aVar = array(
			'friend_id' => $jFriendId,
			'bx_repeat:friends' => $aParsedList,
		);
		
		switch($aData['network']){
			case 'tumblr':
			case 'linkedin':
			case 'live':
			case 'google':
				bx_import('BxDolEmailTemplates');
				$oEmailTemplates = new BxDolEmailTemplates();
				$aMessage = $oEmailTemplates->parseTemplate('sk_social_inviter_email', array(
						'SiteUrl' => BX_DOL_URL_ROOT
				));
				$aVar['title'] = $aMessage['subject'];
				$aVar['content'] = $aMessage['body'];
				$aVar['display_title_div'] = '';
			break;
			default:
				$aVar['title'] = '';
				$aVar['content'] = getParam('sk_social_inviter_invitation_message');
				$aVar['display_title_div'] = 'display:none';
			break;
		}
		
		$sHtmlMessageForm = $this->_oTemplate->parseHtmlByName('message_form.html',$aVar);
		
		$sCaptionItem = <<<BLAH
		<div class="dbTopMenu">
			<i class="login_ajx_close sys-icon remove"></i>
		</div>
BLAH;
		
		$sMessageFormAjx = $GLOBALS['oFunctions']->transBox(DesignBoxContent(_t('_sk_social_inviter_block'), $sHtmlMessageForm, 11, $sCaptionItem), true);
		
		return $sMessageFormAjx;
	}
	
	function genFriendListBlock($jFriendList){
		$session = BxDolSession::getInstance();
		$aData = $session->getValue('si_data');
		
		$aFriendList = json_decode($jFriendList,true);
		
		if($aData['network'] == 'google' || $aData['network'] == 'live'){
			$aExistedFriend = array();
			$count = 0;
			foreach($aFriendList as $key=>$value){
				if(!empty($value['email'])){
					$aProfile = $this->_oDb->getProfileByEmail($value['email']);
					if($aProfile){
						$aExistedFriend[$count]['e_id'] = $aProfile['ID'];
						$aExistedFriend[$count]['e_email'] = $aProfile['Email'];
						$aExistedFriend[$count]['e_username'] = $aProfile['NickName'];
						$aExistedFriend[$count]['e_display_btn'] = !isFriendRequest($this->_iVisitorID,$aProfile['ID']) ? 'display:none':'';
						$count++;
					}
				}
			}
		}
		
		
		$aVar = array(
			'total_invitations' => getParam('sk_social_inviter_quantity_invitation') - $this->_oDb->getTotalInvitations($this->_iVisitorID),
			'total_friends' => $this->_oDb->getTotalFriends($aData['network'],$this->_iVisitorID),
			'network' => $aData['network'],
			'bx_repeat:friends' => $aFriendList,
		);
		if(!empty($aExistedFriend)){
			$aVar['bx_repeat:e_friends'] = $aExistedFriend;
			$aVar['display_e_friend'] = '';
			$aVar['display_e_message'] = 'display:none;';
			$aVar['e_message'] = '';
			$aVar['loading_img'] = $this->_oTemplate->getImageUrl('updating.gif');
			$aVar['display_loading_img'] = 'display:none';
		}else{
			$aVar['display_e_friend'] = 'display:none;';
			$aVar['display_e_message'] = '';
			$aVar['e_message'] = 'You dont have any '.ucfirst($aData['network']).' friend on '.getParam('site_title');	
			$aVar['loading_img'] = '';
			$aVar['display_loading_img'] = '';
		}
		
		$sHtmlFriendList = $this->_oTemplate->parseHtmlByName('friend_list.html',$aVar);
		
		$sCaptionItem = <<<BLAH
		<div class="dbTopMenu">
			<i class="login_ajx_close sys-icon remove"></i>
		</div>
BLAH;
		
		$sFriendListBlockAjx = $GLOBALS['oFunctions']->transBox(DesignBoxContent(_t('_sk_social_inviter_block'), $sHtmlFriendList, 11, $sCaptionItem), true);
		
		return $sFriendListBlockAjx;
	}
	
	function templateAction($sPage, $sPageName)
	{
		bx_import($sPage, $this->_aModule);
		$sClass = $this->_aModule['class_prefix'] . $sPage;
		$oPage = new $sClass($this);
		$this->_oTemplate->pageStart();
		echo $oPage->getCode();
		$this->_oTemplate->pageCode($sPageName, false, false);
	}
	
	private function checkLogged(){
		if($this->_iVisitorID == 0)
		{
			$sUrl = BX_DOL_URL_ROOT;
			header("Location: {$sUrl}");
			exit;
		}
	}
	
	private function getReceiver($sNetwork,$aFriendInfo){
		switch($sNetwork){
			case 'lastfm':
				return $aFriendInfo['username'];
			case 'linkedin':
			case 'tumblr':
			case 'mailru':
			case 'twitter':
			case 'plurk':
				return $aFriendInfo['id'];
			case 'google':
			case 'live':
				return $aFriendInfo['email'];
		}
	}
	
	private function makeSendMessageRequest($sNetwork,$sToken,$aPostData){
		$sRequestUrl = $this->_oConfig->getApiServiceUrl().$sNetwork.'/sendmessage?sk_token='.$sToken;
		$oContext = stream_context_create(array(
			'http' => array(
				'method' => 'POST',
				'header' => 'Content-type: application/x-www-form-urlencoded',
				'content' => http_build_query($aPostData),
			),
		));
		return json_decode(file_get_contents($sRequestUrl,false,$oContext),true);
	}
	
	private function sendMail($sEmail){
		bx_import('BxDolEmailTemplates');
		$oEmailTemplates = new BxDolEmailTemplates();
		$aMessage = $oEmailTemplates->parseTemplate('sk_social_inviter_email', array(
				'SiteUrl' => BX_DOL_URL_ROOT
		));
		return sendMail($sEmail,$aMessage['subject'],$aMessage['body']);
	}
	
	private function isAdmin()
	{
		return isAdmin($this->_iVisitorID) || isModerator($this->_iVisitorID);
	}
}

?>
