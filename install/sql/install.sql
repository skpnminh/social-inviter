CREATE TABLE IF NOT EXISTS `[db_prefix]users` (
	`id` int(11) unsigned not null auto_increment,
	`network` varchar(50) not null,
	`friend_identity` varchar(100) NOT NULL DEFAULT '',
	`profile_id` int(11) unsigned not null,
	primary key(`id`)
)ENGINE=MyISAM  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `[db_prefix]networks` (
  `name` varchar(10) NOT NULL,
  `logo` varchar(50) NOT NULL,
  `status` enum('enabled','disabled') NOT NULL default 'enabled',
  `description` varchar(255) NOT NULL,
  `profile_url` varchar(255) NOT NULL,
  `order` int(2) NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT IGNORE INTO `[db_prefix]networks` (`name`, `logo`, `description`,`profile_url` , `order`) VALUES
('lastfm', 'lastfm.png', 'lastfm logo','http://www.last.fm/user/', 2),
('linkedin', 'linkedin.png', 'linkedin logo','http://www.linkedin.com/', 3),
('twitter', 'twitter.png', 'twitter logo','http://www.twitter.com/', 4),
('tumblr', 'tumblr.png', 'tumblr logo','http://www.tumblr.com/', 5),
('plurk', 'plurk.png', 'plurk logo','http://www.plurk.com/', 6),
('mailru', 'mailru.png', 'mailru logo','http://my.mail.ru/', 7),
('google', 'google.png', 'google logo','http://plus.google.com/', 8),
('reddit', 'reddit.png', 'reddit logo','http://www.reddit.com/user/', 9),
('live', 'live.png', 'live logo','http://profile.live.com/', 10);


-- permalink
INSERT IGNORE INTO `sys_permalinks` VALUES 
(NULL, 'modules/?r=social_inviter', 'm/social_inviter', 'sk_social_inviter_permalinks'),
(NULL, 'modules/?r=social_inviter/', 'm/social_inviter/', 'sk_social_inviter_permalinks');

INSERT IGNORE INTO `sys_options` (`Name`, `VALUE`, `kateg`, `desc`, `Type`, `check`, `err_text`, `order_in_kateg`) VALUES
('sk_social_inviter_permalinks', 'on', 26, 'Enable friendly sandklock social inviter permalinks', 'checkbox', '', '', NULL);

-- top menu 
SET @iTopMenuLastOrder := (SELECT `Order` + 1 FROM `sys_menu_top` WHERE `Parent` = 0 ORDER BY `Order` DESC LIMIT 1);
INSERT IGNORE INTO `sys_menu_top`(`ID`, `Parent`, `Name`, `Caption`, `Link`, `Order`, `Visible`, `Target`, `Onclick`, `Check`, `Editable`, `Deletable`, `Active`, `Type`, `Picture`, `Icon`, `BQuickLink`, `Statistics`) VALUES
(NULL, 0, 'Sandklock Social Inviter', '_sk_social_inviter_top_menu', 'modules/?r=social_inviter|modules/?r=social_inviter/home', @iTopMenuLastOrder, 'non,memb', '', '', '', 1, 1, 1, 'top', 'modules/sandklock/social_inviter/|si_logo1_32.png', '', 1, '');

-- admin menu
SET @iMax = (SELECT MAX(`order`) FROM `sys_menu_admin` WHERE `parent_id` = '2');
INSERT IGNORE INTO `sys_menu_admin` (`parent_id`, `name`, `title`, `url`, `description`, `icon`, `order`) VALUES
(2, 'Sandklock Social Inviter', '_sk_social_inviter_admin_menu', '{siteUrl}modules/?r=social_inviter/administration/', 'Social Inviter module by Sandklock', 'modules/sandklock/social_inviter/|si_logo1.png', @iMax+1);

-- blocks on index,profile page

INSERT INTO `sys_page_compose` (`ID`, `Page`, `PageWidth`, `Desc`, `Caption`, `Column`, `Order`, `Func`, `Content`, `DesignBox`, `ColWidth`, `Visible`, `MinWidth`,`Cache`) VALUES
(NULL, 'profile', '1140px', 'Social Inviter block', '_sk_social_inviter_profile_block', 2, 0, 'PHP', 'return BxDolService::call(''social_inviter'', ''gen_inviter_block'', array($this->oProfileGen->_iProfileID));', 1, 71.9, 'memb', 0,0),
(NULL, 'index', '1140px', 'Social Inviter block', '_sk_social_inviter_index_block', 1, 0, 'PHP', 'return BxDolService::call(''social_inviter'', ''gen_inviter_block'');', 1, 71.9, 'memb', 0,0);

-- action button

INSERT INTO `sys_objects_actions` (`ID`, `Caption`, `Icon`, `Url`, `Script`, `Eval`, `Order`, `Type`, `bDisplayInSubMenuHeader`) VALUES
(NULL, '_sk_social_inviter_home_button', 'modules/sandklock/social_inviter/|si_logo1.png', '{module_url}', '', '', 0, '[db_prefix]', 1);

-- settings
SET @iMaxOrder = (SELECT `menu_order` + 1 FROM `sys_options_cats` ORDER BY `menu_order` DESC LIMIT 1);
INSERT INTO `sys_options_cats` (`name`, `menu_order`) VALUES ('Sandklock Social Inviter Setting', @iMaxOrder+1);
SET @iCategId = (SELECT LAST_INSERT_ID());
INSERT IGNORE INTO `sys_options` (`Name`, `VALUE`, `kateg`, `desc`, `Type`, `check`, `err_text`, `order_in_kateg`, `AvailableValues`)  VALUES
('sk_social_inviter_invitation_message', 'I have found out an amazing website. Join it now!', @iCategId, 'Invitation Message', 'text', '', '', 1, ''),
('sk_social_inviter_quantity_invitation', '1000', @iCategId, 'Maximum invitation member can send', 'digit', '', '', 2, '');


SET @iMaxOrder = (SELECT `menu_order` + 1 FROM `sys_options_cats` ORDER BY `menu_order` DESC LIMIT 1);
INSERT INTO `sys_options_cats` (`name`, `menu_order`) VALUES ('Sandklock Social Inviter Api Setting', @iMaxOrder);
SET @iCategId = (SELECT LAST_INSERT_ID());
INSERT IGNORE INTO `sys_options` (`Name`, `VALUE`, `kateg`, `desc`, `Type`, `check`, `err_text`, `order_in_kateg`, `AvailableValues`)  VALUES
('sk_social_inviter_app_id', '11', @iCategId, 'Module application id', 'digit', '', '', 1, ''),
('sk_social_inviter_api_login_url', 'http://siapi.herokuapp.com/login/', @iCategId, 'Api login url', 'digit', '', '', 2, ''),
('sk_social_inviter_api_service_url', 'http://siapi.herokuapp.com/service/', @iCategId, 'Api service url', 'digit', '', '', 4, '');

-- email template

INSERT INTO `sys_email_templates`(`Name`, `Subject`, `Body`, `Desc`, `LangID`) VALUES
('sk_social_inviter_email', 'An interesting website', 'I have found an amazing website <SiteUrl>, check it out!', 'Social Inviter Template', '0');

-- injections

INSERT INTO `sys_injections` (`name`, `page_index`, `key`, `type`, `data`, `replace`, `active`) VALUES 
('social_inviter_popup', '0', 'injection_footer', 'text', '<script>$.getScript(site_url+"modules/sandklock/social_inviter/js/social_inviter.js")</script>', 0, 1);