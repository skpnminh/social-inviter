<?

$sLangCategory = 'Sk Social Inviter';

$aLangContent = array(
	'_sk_social_inviter' => 'Social Inviter',
	'_sk_social_inviter_top_menu' => 'Social Inviter',
	'_sk_social_inviter_admin_menu' => 'Social Inviter',
	'_sk_social_inviter_home_button' => 'Social Inviter',
	'_sk_social_inviter_index_block' => 'Social Inviter',
	'_sk_social_inviter_profile_block' => 'Social Inviter',
	'_sk_social_inviter_home_page' => 'Social Inviter',
	'_sk_social_inviter_db_error' => 'Database error',
	'_sk_social_inviter_caption_settings' => 'Settings',
	'_sk_social_inviter_caption_api_settings' => 'Api Settings',
	'_sk_social_inviter_caption_manage_network' => 'Manage Networks',
	'_sk_social_inviter_title_admin' => 'Social Inviter',
	'_sk_social_inviter_page_admin' => 'Social Inviter',
	'_sk_social_inviter_block' => 'Social Inviter',
);

?>
