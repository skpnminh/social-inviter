
function showSocialInviterBlock(friends){

	var oPopupOptions = {fog: {color: '#fff', opacity: .7}};
	
	if ($('#sk_friend_block').length){
		$('#sk_friend_block').remove();
	}
	$('<div id="sk_friend_block" style="visibility: none;"></div>').prependTo('body').load(
		site_url + 'm/social_inviter/ajax_mode/friend_list_block',
		{
			ajaxmode: 'true',
			list: friends,
			relocate: String(window.location),
		},
		function() {
			$(this).dolPopup(oPopupOptions);
		});
}